Discovery of R...
========================================================
author: 
date: 
autosize: true

<style>
.reveal .slides{
    width: 90% !important;
}
</style>

R as a calculator
========================================================


```r
2 + 5.627
```

```
[1] 7.627
```

```r
# the result has been computed, but has NOT been saved in the environment

x <- 3 
x # the object x has been created, and it contains the number 3
```

```
[1] 3
```

```r
y <- sqrt(x) 
y # the square root of x
```

```
[1] 1.732051
```


Elementary types
========================================================


```r
x <- 3.69
# the variable x contains a NUMBER 3.69

s1 <- "3.69"
# the variable s1 contains a TEXT that is 3.69
s2 <- "pierre"
# the variable s2 contains a TEXT that is pierre

b1 <- FALSE   # a boolean, the same as b1 <- F
b1 <- T       # the same as b1 <- TRUE
```

(Comparisons)
========================================================

```r
b3 <- x>2
b3
```

```
[1] TRUE
```

```r
b4 <- s2 == "pierre"
b4
```

```
[1] TRUE
```

```r
b4 <- s1 != "pierre"
b4
```

```
[1] TRUE
```


Vectors
========================================================


```r
v1 <- c(3,8,25,4,9,1.5,71,20,5,0.02)
v1
```

```
 [1]  3.00  8.00 25.00  4.00  9.00  1.50 71.00 20.00  5.00  0.02
```

```r
length(v1)
```

```
[1] 10
```

Vectors
========================================================


```r
v2 <- 1:15
v2
```

```
 [1]  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
```

```r
v3 <- seq(0.1,0.5, by=0.1)
# the suite starts at 0.1, ends at 0.5 and the step size between two consecutive values is 0.1
v3
```

```
[1] 0.1 0.2 0.3 0.4 0.5
```

```r
v4 <- seq(50,90, length=15)
# the suite starts at 0.1, ends at 0.5 and I don't provide any step size, but I want 100 values in the interval, so R will compute the step size by itself.
v4
```

```
 [1] 50.00000 52.85714 55.71429 58.57143 61.42857 64.28571 67.14286 70.00000
 [9] 72.85714 75.71429 78.57143 81.42857 84.28571 87.14286 90.00000
```


Operations on Vectors
========================================================

```r
5*v1
```

```
 [1]  15.0  40.0 125.0  20.0  45.0   7.5 355.0 100.0  25.0   0.1
```

```r
# each element of v1 has been multiplied  by 5, and the result is still a vector
5*v1+10
```

```
 [1]  25.0  50.0 135.0  30.0  55.0  17.5 365.0 110.0  35.0  10.1
```

```r
# each element of v1 has been transformed by the function f(x)=5x+10, and the result is still a vector
v1 > 5
```

```
 [1] FALSE  TRUE  TRUE FALSE  TRUE FALSE  TRUE  TRUE FALSE FALSE
```

```r
# each element of v1 has been compared to 5, and the result is a vector of TRUE or FALSE
```


Extracting value(s) from Vectors
========================================================

```r
v1[3]
```

```
[1] 25
```

```r
v1[5:8]
```

```
[1]  9.0  1.5 71.0 20.0
```

```r
# Question: what does the command :
which(v1 > 5)
```

```
[1] 2 3 5 7 8
```

```r
# if you need help, you can type ?which in your console

v1[which(v1 > 5)]
```

```
[1]  8 25  9 71 20
```


Exercise
========================================================

- Extract from v1 all the elements that are multiple of 2 ;
- Extract from v1 all the values less or equal to 10.


The sum command : be careful
========================================================

```r
sum(v1) # the sum of all the elemnts of v1
```

```
[1] 146.52
```

```r
sum(v1>5) # counts the number of elements in v1 such as the element is >5
```

```
[1] 5
```


Manipulating the vectors
========================================================


```r
v1 <- 10:20
v1
```

```
 [1] 10 11 12 13 14 15 16 17 18 19 20
```

```r
v1 + 2
```

```
 [1] 12 13 14 15 16 17 18 19 20 21 22
```

```r
v1 - mean(v1)
```

```
 [1] -5 -4 -3 -2 -1  0  1  2  3  4  5
```

```r
v2 <- c(0,10)
v2
```

```
[1]  0 10
```

```r
v1 + v2
```

```
 [1] 10 21 12 23 14 25 16 27 18 29 20
```


Manipulating the vectors
========================================================


```r
v1 <- 10:20
v1
```

```
 [1] 10 11 12 13 14 15 16 17 18 19 20
```

```r
v2 <- c(1,2,3,4)
v2
```

```
[1] 1 2 3 4
```

```r
v1 + v2
```

```
 [1] 11 13 15 17 15 17 19 21 19 21 23
```


Manipulating the vectors
========================================================


```r
v1 <- 10:20
v1
```

```
 [1] 10 11 12 13 14 15 16 17 18 19 20
```

```r
v1 * 2
```

```
 [1] 20 22 24 26 28 30 32 34 36 38 40
```

```r
v1 ** 2
```

```
 [1] 100 121 144 169 196 225 256 289 324 361 400
```


Exercise
========================================================

- Compute the variance of v1 with only sum and length functions...

$$ \sigma^2 = \frac{1}{(n-1)} \sum{(x_i - m)^2}$$

where :
- $n$ is the size of the obervations ;
- $m$ is the mean




