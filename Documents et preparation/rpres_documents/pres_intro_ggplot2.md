Introduction to ggplot2
========================================================
author: 
date: 
autosize: true

<style>
.reveal .slides{
    width: 90% !important;
}
</style>

Introduction
========================================================

An effective chart is one that:

- Conveys the right information without distorting facts.
- Is simple but elegant. It should not force you to think much in order to get it.
- Aesthetics supports information rather that overshadow it.
- Is not overloaded with information.

The list below sorts the visualizations based on its primary purpose.

Library import...
=======================================================

```r
library(ggplot2)
```

Correlation with scatterplots
=======================================================


```r
# Dataset
data("midwest", package = "ggplot2")
colnames(midwest)
```

```
 [1] "PID"                  "county"               "state"               
 [4] "area"                 "poptotal"             "popdensity"          
 [7] "popwhite"             "popblack"             "popamerindian"       
[10] "popasian"             "popother"             "percwhite"           
[13] "percblack"            "percamerindan"        "percasian"           
[16] "percother"            "popadults"            "perchsd"             
[19] "percollege"           "percprof"             "poppovertyknown"     
[22] "percpovertyknown"     "percbelowpoverty"     "percchildbelowpovert"
[25] "percadultpoverty"     "percelderlypoverty"   "inmetro"             
[28] "category"            
```

Simple scatterplot...
========================================================


```r
gg <- ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_point()
# plot(gg)
```

Simple scatterplot...
========================================================

![plot of chunk unnamed-chunk-4](pres_intro_ggplot2-figure/unnamed-chunk-4-1.png)


Axis delimitation with xlim and ylim
========================================================


```r
gg <- ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_point() +
  xlim(c(0,0.03)) +
  ylim(c(0,5e5))
#plot(gg)
```


Axis delimitation with xlim and ylim
========================================================

![plot of chunk unnamed-chunk-6](pres_intro_ggplot2-figure/unnamed-chunk-6-1.png)



Changing titles and labels with labs
========================================================


```r
gg <- ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_point() +
  labs(subtitle="Area Vs Population", 
       y="Population", 
       x="Area", 
       title="Scatterplot", 
       caption = "Source: midwest")
#plot(gg)
```


Changing titles and labels with labs
========================================================

![plot of chunk unnamed-chunk-8](pres_intro_ggplot2-figure/unnamed-chunk-8-1.png)


Shaking points with jitter
========================================================


```r
gg <- ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_jitter() +
  xlim(c(0,0.03)) +
  ylim(c(0,5e5))
#plot(gg)
```


Shaking points with jitter
========================================================

![plot of chunk unnamed-chunk-10](pres_intro_ggplot2-figure/unnamed-chunk-10-1.png)


Coloring points based on another qualitative variable
========================================================


```r
gg <- ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_jitter(aes(col=state)) +
  xlim(c(0,0.03)) +
  ylim(c(0,5e5))
#plot(gg)
```


Coloring points based on another qualitative variable
========================================================

![plot of chunk unnamed-chunk-12](pres_intro_ggplot2-figure/unnamed-chunk-12-1.png)


Coloring points based on another quantitative variable
========================================================


```r
gg <- ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_jitter(aes(col=percollege)) +
  xlim(c(0,0.03)) +
  ylim(c(0,5e5))
#plot(gg)
```


Coloring points based on another quantitative variable
========================================================

![plot of chunk unnamed-chunk-14](pres_intro_ggplot2-figure/unnamed-chunk-14-1.png)


Changing the colors with a given gradient
========================================================


```r
gg <- ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_jitter(aes(col=percollege)) +
  scale_color_gradient(low="blue", high="red") +
  xlim(c(0,0.03)) +
  ylim(c(0,5e5))
#plot(gg)
```


Changing the colors with a given gradient
========================================================

![plot of chunk unnamed-chunk-16](pres_intro_ggplot2-figure/unnamed-chunk-16-1.png)

Changing the point size
========================================================


```r
gg <- ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_jitter(aes(col=percollege, size=popdensity)) +
  scale_color_gradient(low="blue", high="red") +
  xlim(c(0,0.03)) +
  ylim(c(0,5e5))
#plot(gg)
```


Changing the point size
========================================================

![plot of chunk unnamed-chunk-18](pres_intro_ggplot2-figure/unnamed-chunk-18-1.png)


Exercise 1...
========================================================

With the dataset "data-individuals-large.txt",
propose a scatterplot giving the weight depending on the height.

If you have finished, add :
- color depending on the gender
- dot size depending on the imc



Adding a regression...
========================================================


```r
gg <- ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_point(aes(col=state, size=popdensity)) + 
  geom_smooth(method="loess", se=T) + 
  xlim(c(0, 0.1)) + 
  ylim(c(0, 500000)) + 
  labs(subtitle="Area Vs Population", 
       y="Population", 
       x="Area", 
       title="Scatterplot", 
       caption = "Source: midwest")
# plot(gg)
```

Adding a regression...
========================================================

![plot of chunk unnamed-chunk-20](pres_intro_ggplot2-figure/unnamed-chunk-20-1.png)

Exercise 2...
========================================================

With the dataset "data-individuals.txt",
propose a scatterplot giving the weight depending on the height.

If you have finished, add :
- color depending on the gender
- dot size depending on the imc

Add a regression line with standard error. 

You can try to change regression to "lm" : what happened ?


Simple histogram
==========================================


```r
# Histogram on a Continuous (Numeric) Variable
g <- ggplot(midwest, aes(poptotal)) + 
   geom_histogram( binwidth = 100000, 
                   col="white", 
                   size=.1) +
  xlim(c(0,2e6)) + 
  ylim(c(0,30)) +
  labs(title="Histogram with Fixed Binning")
#plot(g)
```

Simple histogram
==========================================

![plot of chunk unnamed-chunk-22](pres_intro_ggplot2-figure/unnamed-chunk-22-1.png)


Enrichment with colors depending on another variable
==========================================


```r
# Histogram on a Continuous (Numeric) Variable
g <- ggplot(midwest, aes(poptotal)) + 
  scale_fill_brewer(palette = "Spectral") +
  geom_histogram(aes(fill=state), 
                   binwidth = 100000, 
                   col="white", 
                   size=.1) +
  xlim(c(0,2e6)) + 
  ylim(c(0,30)) +
  labs(title="Histogram with Fixed Binning")
#plot(g)
```

Enrichment with colors depending on another variable
==========================================

![plot of chunk unnamed-chunk-24](pres_intro_ggplot2-figure/unnamed-chunk-24-1.png)

Exercise 3...
========================================================

With the dataset "data-individuals-large.txt",
propose a histogram describing the imc distribution.

If you have finished, add :
- titles
- color depending on the gender

Don't hesitate to play with the bin width.



Histogram for a categorical variable
==========================================


```r
df <- read.table("data-individuals.txt", header=TRUE, sep="\t")
# Histogram on a Categorical variable
g <- ggplot(df, aes(x=Major)) + 
  geom_bar() + 
  labs(title="Histogram on Categorical Variable", 
       subtitle="Gender repartition depending on Major") +
  coord_flip()
#plot(g)
```


Histogram for a categorical variable
==========================================

![plot of chunk unnamed-chunk-26](pres_intro_ggplot2-figure/unnamed-chunk-26-1.png)


Enrichment with colors depending on another variable
==========================================


```r
df <- read.table("data-individuals.txt", header=TRUE, sep="\t")
# Histogram on a Categorical variable
g <- ggplot(df, aes(x=Major)) + 
  geom_bar(aes(fill=Gender), width = 0.5) + 
  labs(title="Histogram on Categorical Variable", 
       subtitle="Gender repartition depending on Major") +
  coord_flip()
#plot(g)
```

Enrichment with colors depending on another variable
==========================================

![plot of chunk unnamed-chunk-28](pres_intro_ggplot2-figure/unnamed-chunk-28-1.png)


Exercise 4...
========================================================

With the dataset "data-individuals-large.txt",
propose a barplot describing the glasses repartition depending on the major.

If you have finished, add titles


Simple boxplot
==========================================


```r
g <- ggplot(midwest, aes(y=percasian, x=state)) + 
  geom_boxplot() +
  labs(title="Boxplot",
       subtitle="Percentage of asian people per state")
#plot(g)
```

Simple boxplot
==========================================

![plot of chunk unnamed-chunk-30](pres_intro_ggplot2-figure/unnamed-chunk-30-1.png)


Exercise 5...
========================================================

With the dataset "data-individuals-large.txt",
propose a boxplot the size as a function of the major.

If you have finished :
- add titles
- flip your boxplot
- change colors (one color per major)



(Example with colors)
==========================================


```r
g <- ggplot(midwest, aes(y=percasian, x=state)) + 
  geom_boxplot(aes(col=state)) +
  scale_fill_brewer(palette = "Spectral") +
  labs(title="Boxplot",
       subtitle="Percentage of asian people per state")
#plot(g)
```

(Example with colors)
==========================================

![plot of chunk unnamed-chunk-32](pres_intro_ggplot2-figure/unnamed-chunk-32-1.png)



Violin plot (variant of boxplot)
==========================================


```r
g <- ggplot(midwest, aes(y=percollege, x=state)) + 
  geom_violin(aes(col=state)) +
  scale_fill_brewer(palette = "Spectral") +
  labs(title="Boxplot",
       subtitle="Percentage of asian people per state")
#plot(g)
```


Violin plot (variant of boxplot)
==========================================
![plot of chunk unnamed-chunk-34](pres_intro_ggplot2-figure/unnamed-chunk-34-1.png)


Exercise 6...
========================================================

With the dataset "data-individuals-large.txt", choose :
- one qualitative variable
- one quantitative variable

and propose an appropriate violin plot...


Pie charts
==========================================
These is no specific geom in ggplot2 to build a pie chart...

The trick is :
- to build a unique bar with the counts with geom_bar
- to render it circular with coord_polar

Pie chart
==========================================

```r
library(dplyr)

# Create Data
data <- midwest %>%
  group_by(state) %>%
  summarise(nb = length(county))

# Basic piechart
g <- ggplot(data, aes(x="", y=nb, fill=state)) +
  geom_bar(stat="identity", width=1) +
  coord_polar("y", start=0)
# plot(g)
```

Pie chart
==========================================
![plot of chunk unnamed-chunk-36](pres_intro_ggplot2-figure/unnamed-chunk-36-1.png)

Nicer pie chart
==========================================

```r
library(dplyr)

# Create Data
data <- midwest %>%
  group_by(state) %>%
  summarise(nb = length(county))

# Basic piechart
g <- ggplot(data, aes(x="", y=nb, fill=state)) +
  geom_bar(stat="identity", width=1) +
  coord_polar("y", start=0) + 
  theme_void()
# plot(g)
```

Nicer pie chart
==========================================
![plot of chunk unnamed-chunk-38](pres_intro_ggplot2-figure/unnamed-chunk-38-1.png)

Exercise 7...
========================================================

With the dataset "data-individuals-large.txt", choose :
- one qualitative variable

and propose an appropriate pie chart...


Facetting...
========================================================
Facetting may be used with all geom types.

For example with the graph:


```r
gg <- ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_jitter(aes(col=percollege)) +
  xlim(c(0,0.03)) +
  ylim(c(0,5e5))
```

Facetting...
========================================================


```r
plot(gg) + facet_grid(~state)
```

![plot of chunk unnamed-chunk-40](pres_intro_ggplot2-figure/unnamed-chunk-40-1.png)![plot of chunk unnamed-chunk-40](pres_intro_ggplot2-figure/unnamed-chunk-40-2.png)

Facetting...
========================================================


```r
plot(gg) + facet_grid(state~.)
```

![plot of chunk unnamed-chunk-41](pres_intro_ggplot2-figure/unnamed-chunk-41-1.png)![plot of chunk unnamed-chunk-41](pres_intro_ggplot2-figure/unnamed-chunk-41-2.png)

