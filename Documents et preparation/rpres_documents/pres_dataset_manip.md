Introduction to dataset manipulation
========================================================
author: 
date: 
autosize: true
<style>
.reveal .slides{
    width: 90% !important;
}
</style>

The read.table() function
========================================================

- import a dataset contained in a text file or a spreadsheet. 
- the columns are the variable studied, 
- the lines are the individuals of the population (exactly one individual per line). 

The supported format is very simple : 
- columns are separated with a comma, a space or a tabulation (or anything you want if you specify it with the $sep$ argument).

The resulting object in R is called a $data.frame$.

The read.table() function
========================================================

The supported format is very simple : 
- columns are separated with a comma, a space or a tabulation (or anything you want if you specify it with the $sep$ argument).


```r
# reading the table ...
df <- read.table("data-individuals.txt", header=TRUE, sep="\t")

# looking at the table that has been read (to detect potential errors)
# with at least one of these three commands
head(df)
```

```
   Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters
P1    Mrs 166.1982 61.63149 brown      no         Eco                4
P2    Mrs 163.2252 60.15708  blue     yes         Eco                1
P3    Mrs 168.1762 54.16759  blue      no    Sciences                2
P4    Mrs 162.5219 57.29601 brown     yes Art/Letters                1
P5     Mr 167.3222 70.50401  blue      no         Eco                1
P6    Mrs 159.2243 57.80337 brown      no       Other                0
```


The read.table() function
========================================================

The supported format is very simple : 
- columns are separated with a comma, a space or a tabulation (or anything you want if you specify it with the $sep$ argument).


```r
# reading the table ...
df <- read.table("data-individuals.txt", header=TRUE, sep="\t")

# looking at the table that has been read (to detect potential errors)
# with at least one of these three commands
str(df)
```

```
'data.frame':	122 obs. of  7 variables:
 $ Gender          : Factor w/ 3 levels "Mr","Mrs","NoAnswer": 2 2 2 2 1 2 3 2 1 1 ...
 $ Height          : num  166 163 168 163 167 ...
 $ Weight          : num  61.6 60.2 54.2 57.3 70.5 ...
 $ Eyes            : Factor w/ 3 levels "blue","brown",..: 2 1 1 2 1 2 3 2 1 1 ...
 $ Glasses         : Factor w/ 2 levels "no","yes": 1 2 1 2 1 1 1 1 2 1 ...
 $ Major           : Factor w/ 4 levels "Art/Letters",..: 2 2 4 1 2 3 4 4 1 4 ...
 $ Brothers.Sisters: int  4 1 2 1 1 0 1 0 0 1 ...
```


The read.table() function
========================================================

The supported format is very simple : 
- columns are separated with a comma, a space or a tabulation (or anything you want if you specify it with the $sep$ argument).


```r
# reading the table ...
df <- read.table("data-individuals.txt", header=TRUE, sep="\t")

# looking at the table that has been read (to detect potential errors)
# with at least one of these three commands
summary(df)
```

```
      Gender       Height          Weight         Eyes    Glasses 
 Mr      :48   Min.   :156.7   Min.   :49.22   blue :31   no :82  
 Mrs     :63   1st Qu.:164.4   1st Qu.:58.00   brown:84   yes:40  
 NoAnswer:11   Median :169.7   Median :61.78   green: 7           
               Mean   :172.1   Mean   :65.29                      
               3rd Qu.:179.5   3rd Qu.:74.24                      
               Max.   :194.0   Max.   :84.50                      
         Major    Brothers.Sisters
 Art/Letters:33   Min.   :0.000   
 Eco        :31   1st Qu.:0.000   
 Other      : 4   Median :1.000   
 Sciences   :54   Mean   :1.156   
                  3rd Qu.:2.000   
                  Max.   :4.000   
```

The read.table() function
========================================================

If the command does not work, in 99% of the cases it  is because the filename is not correct ; or the working directory is not correct. 

To indicate your working directory, use the menu 

Session > set working directory > choose directory, 

or the command setwd().



Exploring the table
========================================================
To obtain basic information about the table, you can use the functions :


```r
   nrow(df)    # number of rows in the dataset 
```

```
[1] 122
```

```r
   ncol(df)    # number of columns
```

```
[1] 7
```

```r
   colnames(df)     #  the names of the columns 
```

```
[1] "Gender"           "Height"           "Weight"           "Eyes"            
[5] "Glasses"          "Major"            "Brothers.Sisters"
```

```r
   dim(df)          # dimension of the table 
```

```
[1] 122   7
```

Exploring the table
========================================================
To obtain basic information about the table, you can use the functions :

```r
   summary(df)      # a nice summary of the dataset
```

```
      Gender       Height          Weight         Eyes    Glasses 
 Mr      :48   Min.   :156.7   Min.   :49.22   blue :31   no :82  
 Mrs     :63   1st Qu.:164.4   1st Qu.:58.00   brown:84   yes:40  
 NoAnswer:11   Median :169.7   Median :61.78   green: 7           
               Mean   :172.1   Mean   :65.29                      
               3rd Qu.:179.5   3rd Qu.:74.24                      
               Max.   :194.0   Max.   :84.50                      
         Major    Brothers.Sisters
 Art/Letters:33   Min.   :0.000   
 Eco        :31   1st Qu.:0.000   
 Other      : 4   Median :1.000   
 Sciences   :54   Mean   :1.156   
                  3rd Qu.:2.000   
                  Max.   :4.000   
```




Base-R : Columns selections
========================================================

To select one or several column(s), two solutions are possible:
- The first is to use the name of the column (if there are headers of course)
- The second method is to give the number of the column (be careful, number starts at $1$).


```r
#     1.  selecting a col with its name
#         the output is a vector
df$Weight  
```

```
  [1] 61.63149 60.15708 54.16759 57.29601 70.50401 57.80337 64.83904 53.53604
  [9] 83.96927 77.50725 56.81338 76.64917 80.28017 65.19595 77.07895 64.58050
 [17] 77.62541 76.95144 66.56004 58.65045 60.96286 49.21985 57.02508 58.59332
 [25] 56.05883 76.67326 60.32076 69.24258 69.87804 77.39428 84.50051 57.21836
 [33] 75.25261 58.84697 70.59522 78.11396 54.20974 73.53905 67.47170 61.92590
 [41] 75.11686 58.44982 60.83944 82.27142 57.12178 67.80838 57.65643 55.34621
 [49] 57.81468 71.93520 68.81309 77.83333 50.75216 79.28756 54.74572 79.38409
 [57] 72.02892 50.35163 62.90806 71.26351 53.79586 83.27874 57.93809 72.78892
 [65] 52.85885 80.09708 71.02728 60.67255 58.39448 59.52288 72.46049 58.92606
 [73] 58.80106 74.38827 54.38962 79.76967 59.55820 74.74333 54.68543 79.73289
 [81] 72.39170 53.49748 76.32341 54.89504 73.81172 59.68636 61.27757 56.22138
 [89] 73.65827 74.79687 59.68393 75.08759 69.00997 58.85714 58.44138 63.21528
 [97] 65.67877 58.93534 60.57769 59.67245 59.65035 75.93327 57.34522 63.38222
[105] 64.03747 55.87103 74.93243 57.65877 74.73235 58.00097 75.57279 59.43846
[113] 57.33559 60.80304 58.32844 61.31778 56.62610 59.97037 57.99677 76.91068
[121] 61.97262 63.67826
```

```r
df$Major
```

```
  [1] Eco         Eco         Sciences    Art/Letters Eco         Other      
  [7] Sciences    Sciences    Art/Letters Sciences    Other       Art/Letters
 [13] Art/Letters Sciences    Eco         Sciences    Eco         Sciences   
 [19] Sciences    Art/Letters Art/Letters Sciences    Art/Letters Sciences   
 [25] Sciences    Sciences    Art/Letters Sciences    Sciences    Eco        
 [31] Art/Letters Art/Letters Sciences    Sciences    Eco         Sciences   
 [37] Sciences    Sciences    Sciences    Sciences    Eco         Eco        
 [43] Eco         Sciences    Sciences    Sciences    Sciences    Eco        
 [49] Art/Letters Art/Letters Eco         Sciences    Art/Letters Eco        
 [55] Art/Letters Sciences    Sciences    Art/Letters Eco         Sciences   
 [61] Sciences    Sciences    Eco         Sciences    Art/Letters Eco        
 [67] Eco         Sciences    Art/Letters Art/Letters Eco         Sciences   
 [73] Art/Letters Eco         Art/Letters Eco         Sciences    Other      
 [79] Sciences    Eco         Sciences    Art/Letters Sciences    Eco        
 [85] Sciences    Art/Letters Art/Letters Sciences    Sciences    Sciences   
 [91] Sciences    Art/Letters Sciences    Art/Letters Sciences    Art/Letters
 [97] Eco         Sciences    Sciences    Sciences    Eco         Sciences   
[103] Art/Letters Eco         Eco         Art/Letters Art/Letters Sciences   
[109] Art/Letters Sciences    Sciences    Eco         Eco         Eco        
[115] Sciences    Art/Letters Sciences    Art/Letters Art/Letters Eco        
[121] Other       Eco        
Levels: Art/Letters Eco Other Sciences
```


Base-R : Columns selections
========================================================

To select one or several column(s), two solutions are possible:
- The first is to use the name of the column (if there are headers of course)
- The second method is to give the number of the column (be careful, number starts at $1$).


```r
#     2.  selecting a col with its indices (indices start at 1)
#         the output is a vector
df[,3]
```

```
  [1] 61.63149 60.15708 54.16759 57.29601 70.50401 57.80337 64.83904 53.53604
  [9] 83.96927 77.50725 56.81338 76.64917 80.28017 65.19595 77.07895 64.58050
 [17] 77.62541 76.95144 66.56004 58.65045 60.96286 49.21985 57.02508 58.59332
 [25] 56.05883 76.67326 60.32076 69.24258 69.87804 77.39428 84.50051 57.21836
 [33] 75.25261 58.84697 70.59522 78.11396 54.20974 73.53905 67.47170 61.92590
 [41] 75.11686 58.44982 60.83944 82.27142 57.12178 67.80838 57.65643 55.34621
 [49] 57.81468 71.93520 68.81309 77.83333 50.75216 79.28756 54.74572 79.38409
 [57] 72.02892 50.35163 62.90806 71.26351 53.79586 83.27874 57.93809 72.78892
 [65] 52.85885 80.09708 71.02728 60.67255 58.39448 59.52288 72.46049 58.92606
 [73] 58.80106 74.38827 54.38962 79.76967 59.55820 74.74333 54.68543 79.73289
 [81] 72.39170 53.49748 76.32341 54.89504 73.81172 59.68636 61.27757 56.22138
 [89] 73.65827 74.79687 59.68393 75.08759 69.00997 58.85714 58.44138 63.21528
 [97] 65.67877 58.93534 60.57769 59.67245 59.65035 75.93327 57.34522 63.38222
[105] 64.03747 55.87103 74.93243 57.65877 74.73235 58.00097 75.57279 59.43846
[113] 57.33559 60.80304 58.32844 61.31778 56.62610 59.97037 57.99677 76.91068
[121] 61.97262 63.67826
```



Base-R : Columns selections
========================================================

To select one or several column(s), two solutions are possible:
- The first is to use the name of the column (if there are headers of course)
- The second method is to give the number of the column (be careful, number starts at $1$).


```r
#     3.  selecting multiple col with their names
#         the output is a data.frame
df[, c("Weight","Major")]
```

```
       Weight       Major
P1   61.63149         Eco
P2   60.15708         Eco
P3   54.16759    Sciences
P4   57.29601 Art/Letters
P5   70.50401         Eco
P6   57.80337       Other
P7   64.83904    Sciences
P8   53.53604    Sciences
P9   83.96927 Art/Letters
P10  77.50725    Sciences
P11  56.81338       Other
P12  76.64917 Art/Letters
P13  80.28017 Art/Letters
P14  65.19595    Sciences
P15  77.07895         Eco
P16  64.58050    Sciences
P17  77.62541         Eco
P18  76.95144    Sciences
P19  66.56004    Sciences
P20  58.65045 Art/Letters
P21  60.96286 Art/Letters
P22  49.21985    Sciences
P23  57.02508 Art/Letters
P24  58.59332    Sciences
P25  56.05883    Sciences
P26  76.67326    Sciences
P27  60.32076 Art/Letters
P28  69.24258    Sciences
P29  69.87804    Sciences
P30  77.39428         Eco
P31  84.50051 Art/Letters
P32  57.21836 Art/Letters
P33  75.25261    Sciences
P34  58.84697    Sciences
P35  70.59522         Eco
P36  78.11396    Sciences
P37  54.20974    Sciences
P38  73.53905    Sciences
P39  67.47170    Sciences
P40  61.92590    Sciences
P41  75.11686         Eco
P42  58.44982         Eco
P43  60.83944         Eco
P44  82.27142    Sciences
P45  57.12178    Sciences
P46  67.80838    Sciences
P47  57.65643    Sciences
P48  55.34621         Eco
P49  57.81468 Art/Letters
P50  71.93520 Art/Letters
P51  68.81309         Eco
P52  77.83333    Sciences
P53  50.75216 Art/Letters
P54  79.28756         Eco
P55  54.74572 Art/Letters
P56  79.38409    Sciences
P57  72.02892    Sciences
P58  50.35163 Art/Letters
P59  62.90806         Eco
P60  71.26351    Sciences
P61  53.79586    Sciences
P62  83.27874    Sciences
P63  57.93809         Eco
P64  72.78892    Sciences
P65  52.85885 Art/Letters
P66  80.09708         Eco
P67  71.02728         Eco
P68  60.67255    Sciences
P69  58.39448 Art/Letters
P70  59.52288 Art/Letters
P71  72.46049         Eco
P72  58.92606    Sciences
P73  58.80106 Art/Letters
P74  74.38827         Eco
P75  54.38962 Art/Letters
P76  79.76967         Eco
P77  59.55820    Sciences
P78  74.74333       Other
P79  54.68543    Sciences
P80  79.73289         Eco
P81  72.39170    Sciences
P82  53.49748 Art/Letters
P83  76.32341    Sciences
P84  54.89504         Eco
P85  73.81172    Sciences
P86  59.68636 Art/Letters
P87  61.27757 Art/Letters
P88  56.22138    Sciences
P89  73.65827    Sciences
P90  74.79687    Sciences
P91  59.68393    Sciences
P92  75.08759 Art/Letters
P93  69.00997    Sciences
P94  58.85714 Art/Letters
P95  58.44138    Sciences
P96  63.21528 Art/Letters
P97  65.67877         Eco
P98  58.93534    Sciences
P99  60.57769    Sciences
P100 59.67245    Sciences
P101 59.65035         Eco
P102 75.93327    Sciences
P103 57.34522 Art/Letters
P104 63.38222         Eco
P105 64.03747         Eco
P106 55.87103 Art/Letters
P107 74.93243 Art/Letters
P108 57.65877    Sciences
P109 74.73235 Art/Letters
P110 58.00097    Sciences
P111 75.57279    Sciences
P112 59.43846         Eco
P113 57.33559         Eco
P114 60.80304         Eco
P115 58.32844    Sciences
P116 61.31778 Art/Letters
P117 56.62610    Sciences
P118 59.97037 Art/Letters
P119 57.99677 Art/Letters
P120 76.91068         Eco
P121 61.97262       Other
P122 63.67826         Eco
```

Base-R : Columns selections
========================================================

To select one or several column(s), two solutions are possible:
- The first is to use the name of the column (if there are headers of course)
- The second method is to give the number of the column (be careful, number starts at $1$).


```r
#     4.  selecting multiple col with their indices (indices start at 1)
#         the output is a data.frame
df[, c(3,5)]
```

```
       Weight Glasses
P1   61.63149      no
P2   60.15708     yes
P3   54.16759      no
P4   57.29601     yes
P5   70.50401      no
P6   57.80337      no
P7   64.83904      no
P8   53.53604      no
P9   83.96927     yes
P10  77.50725      no
P11  56.81338      no
P12  76.64917      no
P13  80.28017     yes
P14  65.19595     yes
P15  77.07895      no
P16  64.58050     yes
P17  77.62541      no
P18  76.95144      no
P19  66.56004     yes
P20  58.65045      no
P21  60.96286      no
P22  49.21985      no
P23  57.02508      no
P24  58.59332      no
P25  56.05883      no
P26  76.67326     yes
P27  60.32076     yes
P28  69.24258      no
P29  69.87804      no
P30  77.39428     yes
P31  84.50051     yes
P32  57.21836     yes
P33  75.25261      no
P34  58.84697      no
P35  70.59522      no
P36  78.11396      no
P37  54.20974      no
P38  73.53905     yes
P39  67.47170     yes
P40  61.92590     yes
P41  75.11686     yes
P42  58.44982     yes
P43  60.83944      no
P44  82.27142     yes
P45  57.12178     yes
P46  67.80838      no
P47  57.65643     yes
P48  55.34621      no
P49  57.81468      no
P50  71.93520     yes
P51  68.81309      no
P52  77.83333      no
P53  50.75216      no
P54  79.28756      no
P55  54.74572      no
P56  79.38409      no
P57  72.02892     yes
P58  50.35163      no
P59  62.90806      no
P60  71.26351     yes
P61  53.79586     yes
P62  83.27874      no
P63  57.93809     yes
P64  72.78892     yes
P65  52.85885     yes
P66  80.09708      no
P67  71.02728      no
P68  60.67255      no
P69  58.39448      no
P70  59.52288      no
P71  72.46049      no
P72  58.92606      no
P73  58.80106      no
P74  74.38827      no
P75  54.38962      no
P76  79.76967      no
P77  59.55820      no
P78  74.74333     yes
P79  54.68543     yes
P80  79.73289     yes
P81  72.39170      no
P82  53.49748      no
P83  76.32341     yes
P84  54.89504      no
P85  73.81172      no
P86  59.68636      no
P87  61.27757      no
P88  56.22138      no
P89  73.65827     yes
P90  74.79687      no
P91  59.68393      no
P92  75.08759      no
P93  69.00997      no
P94  58.85714     yes
P95  58.44138      no
P96  63.21528      no
P97  65.67877      no
P98  58.93534      no
P99  60.57769      no
P100 59.67245      no
P101 59.65035     yes
P102 75.93327      no
P103 57.34522     yes
P104 63.38222      no
P105 64.03747      no
P106 55.87103      no
P107 74.93243      no
P108 57.65877      no
P109 74.73235     yes
P110 58.00097      no
P111 75.57279      no
P112 59.43846      no
P113 57.33559      no
P114 60.80304     yes
P115 58.32844      no
P116 61.31778     yes
P117 56.62610      no
P118 59.97037      no
P119 57.99677      no
P120 76.91068     yes
P121 61.97262      no
P122 63.67826     yes
```



Base-R : Lines selections
========================================================

To select one or several line(s), two solutions are possible:
- The first method is to give the number(s) of the line(s) (be careful, number starts at $1$).
- The second one is to indicate a condition (useful to sub-select data)


```r
#     1.  selecting a line with its indices (indices start at 1)
#         the output is a list 
df[8,]
```

```
   Gender   Height   Weight  Eyes Glasses    Major Brothers.Sisters
P8    Mrs 164.3455 53.53604 brown      no Sciences                0
```

Base-R : Lines selections
========================================================

To select one or several line(s), two solutions are possible:
- The first method is to give the number(s) of the line(s) (be careful, number starts at $1$).
- The second one is to indicate a condition (useful to sub-select data)


```r
#     2.  selecting multiple lines with their indices (indices start at 1)
#         the output is a data.frame
df[c(1,3,5,7),]
```

```
     Gender   Height   Weight  Eyes Glasses    Major Brothers.Sisters
P1      Mrs 166.1982 61.63149 brown      no      Eco                4
P3      Mrs 168.1762 54.16759  blue      no Sciences                2
P5       Mr 167.3222 70.50401  blue      no      Eco                1
P7 NoAnswer 180.4609 64.83904 green      no Sciences                1
```

```r
df[90:100,]
```

```
     Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters
P90      Mr 182.8616 74.79687 brown      no    Sciences                2
P91     Mrs 164.2055 59.68393 brown      no    Sciences                0
P92      Mr 178.7189 75.08759 brown      no Art/Letters                0
P93      Mr 186.2042 69.00997  blue      no    Sciences                0
P94     Mrs 164.7514 58.85714 brown     yes Art/Letters                0
P95     Mrs 169.7009 58.44138  blue      no    Sciences                0
P96     Mrs 171.0626 63.21528 brown      no Art/Letters                2
P97     Mrs 163.9292 65.67877 brown      no         Eco                0
P98     Mrs 167.9866 58.93534  blue      no    Sciences                0
P99     Mrs 165.9069 60.57769 brown      no    Sciences                2
P100    Mrs 161.8602 59.67245 brown      no    Sciences                2
```

Base-R : Lines selections
========================================================

To select one or several line(s), two solutions are possible:
- The first method is to give the number(s) of the line(s) (be careful, number starts at $1$).
- The second one is to indicate a condition (useful to sub-select data)


```r
#     3.  selecting one or multiple lines given a boolean condition
#         the output is a data.frame
df[which(df$Gender == "Mrs"),]
```

```
     Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters
P1      Mrs 166.1982 61.63149 brown      no         Eco                4
P2      Mrs 163.2252 60.15708  blue     yes         Eco                1
P3      Mrs 168.1762 54.16759  blue      no    Sciences                2
P4      Mrs 162.5219 57.29601 brown     yes Art/Letters                1
P6      Mrs 159.2243 57.80337 brown      no       Other                0
P8      Mrs 164.3455 53.53604 brown      no    Sciences                0
P11     Mrs 172.5316 56.81338 brown      no       Other                1
P20     Mrs 172.5036 58.65045 brown      no Art/Letters                0
P21     Mrs 163.3793 60.96286 brown      no Art/Letters                2
P23     Mrs 165.0174 57.02508 brown      no Art/Letters                1
P24     Mrs 165.4944 58.59332  blue      no    Sciences                1
P25     Mrs 161.5655 56.05883 brown      no    Sciences                1
P27     Mrs 168.5017 60.32076  blue     yes Art/Letters                0
P32     Mrs 165.5190 57.21836 brown     yes Art/Letters                1
P34     Mrs 160.6649 58.84697 brown      no    Sciences                0
P37     Mrs 170.8752 54.20974 brown      no    Sciences                4
P40     Mrs 162.2356 61.92590 brown     yes    Sciences                1
P42     Mrs 161.0769 58.44982 brown     yes         Eco                1
P43     Mrs 165.7316 60.83944 brown      no         Eco                1
P45     Mrs 159.3769 57.12178 brown     yes    Sciences                4
P47     Mrs 168.8310 57.65643 brown     yes    Sciences                1
P48     Mrs 162.6460 55.34621 brown      no         Eco                2
P49     Mrs 169.8175 57.81468  blue      no Art/Letters                1
P53     Mrs 164.6524 50.75216  blue      no Art/Letters                1
P55     Mrs 169.6532 54.74572  blue      no Art/Letters                1
P58     Mrs 162.7387 50.35163  blue      no Art/Letters                0
P61     Mrs 166.0699 53.79586 brown     yes    Sciences                1
P63     Mrs 168.6083 57.93809 brown     yes         Eco                1
P65     Mrs 160.5987 52.85885 brown     yes Art/Letters                2
P68     Mrs 156.6753 60.67255  blue      no    Sciences                0
P69     Mrs 168.2649 58.39448 green      no Art/Letters                1
P70     Mrs 163.5965 59.52288 brown      no Art/Letters                0
P72     Mrs 160.1538 58.92606 brown      no    Sciences                3
P73     Mrs 170.4877 58.80106  blue      no Art/Letters                2
P75     Mrs 161.4072 54.38962 brown      no Art/Letters                0
P77     Mrs 164.4084 59.55820 brown      no    Sciences                0
P79     Mrs 165.2660 54.68543 brown     yes    Sciences                2
P82     Mrs 169.3057 53.49748  blue      no Art/Letters                0
P84     Mrs 162.7082 54.89504 brown      no         Eco                2
P86     Mrs 161.4521 59.68636 brown      no Art/Letters                1
P87     Mrs 160.9725 61.27757 brown      no Art/Letters                0
P88     Mrs 164.9215 56.22138  blue      no    Sciences                0
P91     Mrs 164.2055 59.68393 brown      no    Sciences                0
P94     Mrs 164.7514 58.85714 brown     yes Art/Letters                0
P95     Mrs 169.7009 58.44138  blue      no    Sciences                0
P96     Mrs 171.0626 63.21528 brown      no Art/Letters                2
P97     Mrs 163.9292 65.67877 brown      no         Eco                0
P98     Mrs 167.9866 58.93534  blue      no    Sciences                0
P99     Mrs 165.9069 60.57769 brown      no    Sciences                2
P100    Mrs 161.8602 59.67245 brown      no    Sciences                2
P101    Mrs 166.3038 59.65035 brown     yes         Eco                3
P103    Mrs 166.4414 57.34522 brown     yes Art/Letters                1
P106    Mrs 169.5244 55.87103 brown      no Art/Letters                1
P108    Mrs 161.6986 57.65877 brown      no    Sciences                3
P110    Mrs 168.7058 58.00097  blue      no    Sciences                2
P112    Mrs 164.2079 59.43846 brown      no         Eco                1
P113    Mrs 160.6499 57.33559  blue      no         Eco                4
P114    Mrs 160.2727 60.80304 brown     yes         Eco                1
P115    Mrs 158.8877 58.32844  blue      no    Sciences                2
P117    Mrs 159.0964 56.62610 brown      no    Sciences                0
P118    Mrs 165.2194 59.97037 brown      no Art/Letters                0
P119    Mrs 167.2734 57.99677 brown      no Art/Letters                4
P121    Mrs 161.6951 61.97262 brown      no       Other                1
```

```r
df[which(df$Gender == "Mrs" & df$Height > 170),]
```

```
    Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters
P11    Mrs 172.5316 56.81338 brown      no       Other                1
P20    Mrs 172.5036 58.65045 brown      no Art/Letters                0
P37    Mrs 170.8752 54.20974 brown      no    Sciences                4
P73    Mrs 170.4877 58.80106  blue      no Art/Letters                2
P96    Mrs 171.0626 63.21528 brown      no Art/Letters                2
```

```r
df[which(df$Major %in% c("Eco","Sciences")),]
```

```
       Gender   Height   Weight  Eyes Glasses    Major Brothers.Sisters
P1        Mrs 166.1982 61.63149 brown      no      Eco                4
P2        Mrs 163.2252 60.15708  blue     yes      Eco                1
P3        Mrs 168.1762 54.16759  blue      no Sciences                2
P5         Mr 167.3222 70.50401  blue      no      Eco                1
P7   NoAnswer 180.4609 64.83904 green      no Sciences                1
P8        Mrs 164.3455 53.53604 brown      no Sciences                0
P10        Mr 185.1283 77.50725  blue      no Sciences                1
P14  NoAnswer 180.8256 65.19595 brown     yes Sciences                0
P15        Mr 182.0334 77.07895 brown      no      Eco                1
P16        Mr 184.7760 64.58050  blue     yes Sciences                4
P17        Mr 173.3687 77.62541 brown      no      Eco                2
P18        Mr 175.1551 76.95144 brown      no Sciences                1
P19        Mr 172.0005 66.56004 brown     yes Sciences                0
P22  NoAnswer 171.6157 49.21985 brown      no Sciences                1
P24       Mrs 165.4944 58.59332  blue      no Sciences                1
P25       Mrs 161.5655 56.05883 brown      no Sciences                1
P26        Mr 170.7774 76.67326 brown     yes Sciences                2
P28        Mr 190.7806 69.24258 brown      no Sciences                1
P29        Mr 174.1133 69.87804  blue      no Sciences                2
P30  NoAnswer 178.4980 77.39428 brown     yes      Eco                3
P33        Mr 186.6387 75.25261 brown      no Sciences                1
P34       Mrs 160.6649 58.84697 brown      no Sciences                0
P35  NoAnswer 177.0787 70.59522 brown      no      Eco                2
P36        Mr 173.0950 78.11396 brown      no Sciences                1
P37       Mrs 170.8752 54.20974 brown      no Sciences                4
P38        Mr 190.6344 73.53905 brown     yes Sciences                0
P39        Mr 173.9497 67.47170 brown     yes Sciences                1
P40       Mrs 162.2356 61.92590 brown     yes Sciences                1
P41        Mr 183.6536 75.11686  blue     yes      Eco                1
P42       Mrs 161.0769 58.44982 brown     yes      Eco                1
P43       Mrs 165.7316 60.83944 brown      no      Eco                1
P44        Mr 187.5520 82.27142 brown     yes Sciences                2
P45       Mrs 159.3769 57.12178 brown     yes Sciences                4
P46  NoAnswer 169.2394 67.80838  blue      no Sciences                1
P47       Mrs 168.8310 57.65643 brown     yes Sciences                1
P48       Mrs 162.6460 55.34621 brown      no      Eco                2
P51        Mr 178.3130 68.81309 brown      no      Eco                1
P52        Mr 183.3218 77.83333  blue      no Sciences                0
P54        Mr 178.6433 79.28756 brown      no      Eco                2
P56        Mr 188.5872 79.38409 brown      no Sciences                1
P57        Mr 182.9915 72.02892 brown     yes Sciences                2
P59  NoAnswer 169.1397 62.90806 brown      no      Eco                0
P60        Mr 188.3856 71.26351 brown     yes Sciences                1
P61       Mrs 166.0699 53.79586 brown     yes Sciences                1
P62        Mr 192.2434 83.27874 brown      no Sciences                1
P63       Mrs 168.6083 57.93809 brown     yes      Eco                1
P64        Mr 183.8382 72.78892 brown     yes Sciences                1
P66        Mr 186.9896 80.09708 brown      no      Eco                0
P67        Mr 180.1648 71.02728 green      no      Eco                0
P68       Mrs 156.6753 60.67255  blue      no Sciences                0
P71        Mr 178.0747 72.46049 brown      no      Eco                0
P72       Mrs 160.1538 58.92606 brown      no Sciences                3
P74        Mr 189.5150 74.38827 brown      no      Eco                1
P76        Mr 177.7539 79.76967 brown      no      Eco                2
P77       Mrs 164.4084 59.55820 brown      no Sciences                0
P79       Mrs 165.2660 54.68543 brown     yes Sciences                2
P80        Mr 192.0525 79.73289 green     yes      Eco                1
P81        Mr 178.0660 72.39170 brown      no Sciences                1
P83        Mr 188.6166 76.32341 brown     yes Sciences                0
P84       Mrs 162.7082 54.89504 brown      no      Eco                2
P85        Mr 194.0169 73.81172 brown      no Sciences                1
P88       Mrs 164.9215 56.22138  blue      no Sciences                0
P89        Mr 175.4864 73.65827  blue     yes Sciences                1
P90        Mr 182.8616 74.79687 brown      no Sciences                2
P91       Mrs 164.2055 59.68393 brown      no Sciences                0
P93        Mr 186.2042 69.00997  blue      no Sciences                0
P95       Mrs 169.7009 58.44138  blue      no Sciences                0
P97       Mrs 163.9292 65.67877 brown      no      Eco                0
P98       Mrs 167.9866 58.93534  blue      no Sciences                0
P99       Mrs 165.9069 60.57769 brown      no Sciences                2
P100      Mrs 161.8602 59.67245 brown      no Sciences                2
P101      Mrs 166.3038 59.65035 brown     yes      Eco                3
P102       Mr 184.1869 75.93327 green      no Sciences                0
P104 NoAnswer 173.4296 63.38222 green      no      Eco                1
P105 NoAnswer 161.7861 64.03747 brown      no      Eco                4
P108      Mrs 161.6986 57.65877 brown      no Sciences                3
P110      Mrs 168.7058 58.00097  blue      no Sciences                2
P111       Mr 170.2862 75.57279  blue      no Sciences                2
P112      Mrs 164.2079 59.43846 brown      no      Eco                1
P113      Mrs 160.6499 57.33559  blue      no      Eco                4
P114      Mrs 160.2727 60.80304 brown     yes      Eco                1
P115      Mrs 158.8877 58.32844  blue      no Sciences                2
P117      Mrs 159.0964 56.62610 brown      no Sciences                0
P120       Mr 180.5747 76.91068 brown     yes      Eco                2
P122 NoAnswer 169.1902 63.67826 brown     yes      Eco                0
```



Base-R : Lines selections
========================================================

The last instruction (selection by condition) is the basis of everything in base-R. 

In some programs, some programmers simplify the command by writing it a little bit differently :

```r
# instead of writing :
df[which(df$Weight < 52),]
```

```
      Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters
P22 NoAnswer 171.6157 49.21985 brown      no    Sciences                1
P53      Mrs 164.6524 50.75216  blue      no Art/Letters                1
P58      Mrs 162.7387 50.35163  blue      no Art/Letters                0
```

```r
# they write :
df[df$Weight < 52,]
```

```
      Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters
P22 NoAnswer 171.6157 49.21985 brown      no    Sciences                1
P53      Mrs 164.6524 50.75216  blue      no Art/Letters                1
P58      Mrs 162.7387 50.35163  blue      no Art/Letters                0
```

Even if the results are equivalent with our dataset, the two commands are not equivalent. 


Base-R : Functions for one or two vector(s)
========================================================

A small list here some useful commands :

```r
# functions for one vector of quantitative data
mean(df$Height, na.rm=TRUE) 
```

```
[1] 172.1461
```

```r
sd(df$Height, na.rm=TRUE)     # the standard deviation
```

```
[1] 9.572842
```

```r
median(df$Height, na.rm=TRUE)
```

```
[1] 169.677
```

Base-R : Functions for one or two vector(s)
========================================================

A small list here some useful commands :

```r
# functions for one vector of quantitative data
quantile(df$Height, na.rm=TRUE)
```

```
      0%      25%      50%      75%     100% 
156.6753 164.3612 169.6770 179.5231 194.0169 
```

```r
quantile(df$Height, na.rm=TRUE, probs=seq(0,1,by=0.1))
```

```
      0%      10%      20%      30%      40%      50%      60%      70% 
156.6753 161.4117 163.2560 165.2334 168.0624 169.6770 172.5204 178.0359 
     80%      90%     100% 
181.8962 186.9545 194.0169 
```


Base-R : Functions for one or two vector(s)
========================================================

A small list here some useful commands :

```r
# functions for two vectors of quantitative data
cor(df$Height, df$Weight, method="pearson")
```

```
[1] 0.7763215
```

```r
cor(df$Height, df$Weight, method="spearman")
```

```
[1] 0.7231058
```


Base-R : Functions for one or two vector(s)
========================================================

A small list here some useful commands :

```r
# functions for one vector of qualitative data
table(df$Major)
```

```

Art/Letters         Eco       Other    Sciences 
         33          31           4          54 
```

```r
# functions for two vectors of qualitative data
table(df$Major, df$Glasses)
```

```
             
              no yes
  Art/Letters 21  12
  Eco         21  10
  Other        3   1
  Sciences    37  17
```



Base-R : Functions for one or two vector(s)
========================================================

A small list here some useful commands :

```r
# to count...
length(which(df$Height > 180))
```

```
[1] 30
```


Base-R : Exercises
========================================================

- Upload the dataset "data-individuals-large.txt". Be careful with the read.table() options...
- Calculate the average weight, and the standard deviation of weights.
- What is the average weight of men wearing glasses?
- Calculate the body mass index (bmi), for all the individuals in the table (the formula is given below), and add it as a new column in the data.frame.
$$ bmi = weight / height^2 $$
(the size is in m, the weight in kgs).
- How many persons have a body mass index under 19 ?



Tidyverse R
========================================================

What is the tidyverse ?

The tidyverse is an opinionated collection of R packages designed for data science. All packages share an underlying design philosophy, grammar, and data structures. 

Packages of the tidyverse
========================================================

- tidyr: The goal of tidyr is to help you create tidy data. Tidy data is data where :

        - Every column is variable.
        - Every row is an observation.
        - Every cell is a single value.


- dplyr: a grammar of data manipulation, providing a consistent set of verbs that help you solve the most common data manipulation challenges 

- ggplot2: system for declaratively creating graphics, based on The Grammar of Graphics.

- tibble & purrr


Line(s) selection with the dplyr verb filter
========================================================

```r
library(dplyr)
# a simple selection of lines
df %>%
  filter(Major == "Art/Letters")
```

```
     Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters
1       Mrs 162.5219 57.29601 brown     yes Art/Letters                1
2        Mr 188.3186 83.96927  blue     yes Art/Letters                0
3        Mr 179.7912 76.64917  blue      no Art/Letters                0
4        Mr 177.9655 80.28017 brown     yes Art/Letters                0
5       Mrs 172.5036 58.65045 brown      no Art/Letters                0
6       Mrs 163.3793 60.96286 brown      no Art/Letters                2
7       Mrs 165.0174 57.02508 brown      no Art/Letters                1
8       Mrs 168.5017 60.32076  blue     yes Art/Letters                0
9        Mr 171.6314 84.50051  blue     yes Art/Letters                2
10      Mrs 165.5190 57.21836 brown     yes Art/Letters                1
11      Mrs 169.8175 57.81468  blue      no Art/Letters                1
12       Mr 181.3475 71.93520 brown     yes Art/Letters                0
13      Mrs 164.6524 50.75216  blue      no Art/Letters                1
14      Mrs 169.6532 54.74572  blue      no Art/Letters                1
15      Mrs 162.7387 50.35163  blue      no Art/Letters                0
16      Mrs 160.5987 52.85885 brown     yes Art/Letters                2
17      Mrs 168.2649 58.39448 green      no Art/Letters                1
18      Mrs 163.5965 59.52288 brown      no Art/Letters                0
19      Mrs 170.4877 58.80106  blue      no Art/Letters                2
20      Mrs 161.4072 54.38962 brown      no Art/Letters                0
21      Mrs 169.3057 53.49748  blue      no Art/Letters                0
22      Mrs 161.4521 59.68636 brown      no Art/Letters                1
23      Mrs 160.9725 61.27757 brown      no Art/Letters                0
24       Mr 178.7189 75.08759 brown      no Art/Letters                0
25      Mrs 164.7514 58.85714 brown     yes Art/Letters                0
26      Mrs 171.0626 63.21528 brown      no Art/Letters                2
27      Mrs 166.4414 57.34522 brown     yes Art/Letters                1
28      Mrs 169.5244 55.87103 brown      no Art/Letters                1
29       Mr 173.8025 74.93243  blue      no Art/Letters                1
30       Mr 189.6979 74.73235 brown     yes Art/Letters                0
31 NoAnswer 171.9696 61.31778 green     yes Art/Letters                4
32      Mrs 165.2194 59.97037 brown      no Art/Letters                0
33      Mrs 167.2734 57.99677 brown      no Art/Letters                4
```

Pipe...
========================================================
# %>% is a pipe 

Line(s) selection with the dplyr verb filter
========================================================

```r
# if you have several criteria
df %>%
  filter(Major == "Art/Letters") %>%
  filter(Height >= median(Height))
```

```
     Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters
1        Mr 188.3186 83.96927  blue     yes Art/Letters                0
2        Mr 179.7912 76.64917  blue      no Art/Letters                0
3        Mr 177.9655 80.28017 brown     yes Art/Letters                0
4       Mrs 172.5036 58.65045 brown      no Art/Letters                0
5       Mrs 168.5017 60.32076  blue     yes Art/Letters                0
6        Mr 171.6314 84.50051  blue     yes Art/Letters                2
7       Mrs 169.8175 57.81468  blue      no Art/Letters                1
8        Mr 181.3475 71.93520 brown     yes Art/Letters                0
9       Mrs 169.6532 54.74572  blue      no Art/Letters                1
10      Mrs 170.4877 58.80106  blue      no Art/Letters                2
11      Mrs 169.3057 53.49748  blue      no Art/Letters                0
12       Mr 178.7189 75.08759 brown      no Art/Letters                0
13      Mrs 171.0626 63.21528 brown      no Art/Letters                2
14      Mrs 169.5244 55.87103 brown      no Art/Letters                1
15       Mr 173.8025 74.93243  blue      no Art/Letters                1
16       Mr 189.6979 74.73235 brown     yes Art/Letters                0
17 NoAnswer 171.9696 61.31778 green     yes Art/Letters                4
```

Line(s) selection with the dplyr verb filter
========================================================

```r
# is not equivalent to
df %>%
  filter(Major == "Art/Letters",
         Height >= median(Height))
```

```
     Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters
1        Mr 188.3186 83.96927  blue     yes Art/Letters                0
2        Mr 179.7912 76.64917  blue      no Art/Letters                0
3        Mr 177.9655 80.28017 brown     yes Art/Letters                0
4       Mrs 172.5036 58.65045 brown      no Art/Letters                0
5        Mr 171.6314 84.50051  blue     yes Art/Letters                2
6       Mrs 169.8175 57.81468  blue      no Art/Letters                1
7        Mr 181.3475 71.93520 brown     yes Art/Letters                0
8       Mrs 170.4877 58.80106  blue      no Art/Letters                2
9        Mr 178.7189 75.08759 brown      no Art/Letters                0
10      Mrs 171.0626 63.21528 brown      no Art/Letters                2
11       Mr 173.8025 74.93243  blue      no Art/Letters                1
12       Mr 189.6979 74.73235 brown     yes Art/Letters                0
13 NoAnswer 171.9696 61.31778 green     yes Art/Letters                4
```

Question
========================================================

Why the two previous commands are not equivalent ?

Columns(s) selection with the dplyr verb select
========================================================

```r
# a simple selection of columns
df %>%
  select("Gender","Glasses","Weight")
```

```
       Gender Glasses   Weight
P1        Mrs      no 61.63149
P2        Mrs     yes 60.15708
P3        Mrs      no 54.16759
P4        Mrs     yes 57.29601
P5         Mr      no 70.50401
P6        Mrs      no 57.80337
P7   NoAnswer      no 64.83904
P8        Mrs      no 53.53604
P9         Mr     yes 83.96927
P10        Mr      no 77.50725
P11       Mrs      no 56.81338
P12        Mr      no 76.64917
P13        Mr     yes 80.28017
P14  NoAnswer     yes 65.19595
P15        Mr      no 77.07895
P16        Mr     yes 64.58050
P17        Mr      no 77.62541
P18        Mr      no 76.95144
P19        Mr     yes 66.56004
P20       Mrs      no 58.65045
P21       Mrs      no 60.96286
P22  NoAnswer      no 49.21985
P23       Mrs      no 57.02508
P24       Mrs      no 58.59332
P25       Mrs      no 56.05883
P26        Mr     yes 76.67326
P27       Mrs     yes 60.32076
P28        Mr      no 69.24258
P29        Mr      no 69.87804
P30  NoAnswer     yes 77.39428
P31        Mr     yes 84.50051
P32       Mrs     yes 57.21836
P33        Mr      no 75.25261
P34       Mrs      no 58.84697
P35  NoAnswer      no 70.59522
P36        Mr      no 78.11396
P37       Mrs      no 54.20974
P38        Mr     yes 73.53905
P39        Mr     yes 67.47170
P40       Mrs     yes 61.92590
P41        Mr     yes 75.11686
P42       Mrs     yes 58.44982
P43       Mrs      no 60.83944
P44        Mr     yes 82.27142
P45       Mrs     yes 57.12178
P46  NoAnswer      no 67.80838
P47       Mrs     yes 57.65643
P48       Mrs      no 55.34621
P49       Mrs      no 57.81468
P50        Mr     yes 71.93520
P51        Mr      no 68.81309
P52        Mr      no 77.83333
P53       Mrs      no 50.75216
P54        Mr      no 79.28756
P55       Mrs      no 54.74572
P56        Mr      no 79.38409
P57        Mr     yes 72.02892
P58       Mrs      no 50.35163
P59  NoAnswer      no 62.90806
P60        Mr     yes 71.26351
P61       Mrs     yes 53.79586
P62        Mr      no 83.27874
P63       Mrs     yes 57.93809
P64        Mr     yes 72.78892
P65       Mrs     yes 52.85885
P66        Mr      no 80.09708
P67        Mr      no 71.02728
P68       Mrs      no 60.67255
P69       Mrs      no 58.39448
P70       Mrs      no 59.52288
P71        Mr      no 72.46049
P72       Mrs      no 58.92606
P73       Mrs      no 58.80106
P74        Mr      no 74.38827
P75       Mrs      no 54.38962
P76        Mr      no 79.76967
P77       Mrs      no 59.55820
P78        Mr     yes 74.74333
P79       Mrs     yes 54.68543
P80        Mr     yes 79.73289
P81        Mr      no 72.39170
P82       Mrs      no 53.49748
P83        Mr     yes 76.32341
P84       Mrs      no 54.89504
P85        Mr      no 73.81172
P86       Mrs      no 59.68636
P87       Mrs      no 61.27757
P88       Mrs      no 56.22138
P89        Mr     yes 73.65827
P90        Mr      no 74.79687
P91       Mrs      no 59.68393
P92        Mr      no 75.08759
P93        Mr      no 69.00997
P94       Mrs     yes 58.85714
P95       Mrs      no 58.44138
P96       Mrs      no 63.21528
P97       Mrs      no 65.67877
P98       Mrs      no 58.93534
P99       Mrs      no 60.57769
P100      Mrs      no 59.67245
P101      Mrs     yes 59.65035
P102       Mr      no 75.93327
P103      Mrs     yes 57.34522
P104 NoAnswer      no 63.38222
P105 NoAnswer      no 64.03747
P106      Mrs      no 55.87103
P107       Mr      no 74.93243
P108      Mrs      no 57.65877
P109       Mr     yes 74.73235
P110      Mrs      no 58.00097
P111       Mr      no 75.57279
P112      Mrs      no 59.43846
P113      Mrs      no 57.33559
P114      Mrs     yes 60.80304
P115      Mrs      no 58.32844
P116 NoAnswer     yes 61.31778
P117      Mrs      no 56.62610
P118      Mrs      no 59.97037
P119      Mrs      no 57.99677
P120       Mr     yes 76.91068
P121      Mrs      no 61.97262
P122 NoAnswer     yes 63.67826
```

Columns(s) selection with the dplyr verb select
========================================================

```r
# to remove a precise column
df %>%
  select(-"Gender")
```

```
       Height   Weight  Eyes Glasses       Major Brothers.Sisters
P1   166.1982 61.63149 brown      no         Eco                4
P2   163.2252 60.15708  blue     yes         Eco                1
P3   168.1762 54.16759  blue      no    Sciences                2
P4   162.5219 57.29601 brown     yes Art/Letters                1
P5   167.3222 70.50401  blue      no         Eco                1
P6   159.2243 57.80337 brown      no       Other                0
P7   180.4609 64.83904 green      no    Sciences                1
P8   164.3455 53.53604 brown      no    Sciences                0
P9   188.3186 83.96927  blue     yes Art/Letters                0
P10  185.1283 77.50725  blue      no    Sciences                1
P11  172.5316 56.81338 brown      no       Other                1
P12  179.7912 76.64917  blue      no Art/Letters                0
P13  177.9655 80.28017 brown     yes Art/Letters                0
P14  180.8256 65.19595 brown     yes    Sciences                0
P15  182.0334 77.07895 brown      no         Eco                1
P16  184.7760 64.58050  blue     yes    Sciences                4
P17  173.3687 77.62541 brown      no         Eco                2
P18  175.1551 76.95144 brown      no    Sciences                1
P19  172.0005 66.56004 brown     yes    Sciences                0
P20  172.5036 58.65045 brown      no Art/Letters                0
P21  163.3793 60.96286 brown      no Art/Letters                2
P22  171.6157 49.21985 brown      no    Sciences                1
P23  165.0174 57.02508 brown      no Art/Letters                1
P24  165.4944 58.59332  blue      no    Sciences                1
P25  161.5655 56.05883 brown      no    Sciences                1
P26  170.7774 76.67326 brown     yes    Sciences                2
P27  168.5017 60.32076  blue     yes Art/Letters                0
P28  190.7806 69.24258 brown      no    Sciences                1
P29  174.1133 69.87804  blue      no    Sciences                2
P30  178.4980 77.39428 brown     yes         Eco                3
P31  171.6314 84.50051  blue     yes Art/Letters                2
P32  165.5190 57.21836 brown     yes Art/Letters                1
P33  186.6387 75.25261 brown      no    Sciences                1
P34  160.6649 58.84697 brown      no    Sciences                0
P35  177.0787 70.59522 brown      no         Eco                2
P36  173.0950 78.11396 brown      no    Sciences                1
P37  170.8752 54.20974 brown      no    Sciences                4
P38  190.6344 73.53905 brown     yes    Sciences                0
P39  173.9497 67.47170 brown     yes    Sciences                1
P40  162.2356 61.92590 brown     yes    Sciences                1
P41  183.6536 75.11686  blue     yes         Eco                1
P42  161.0769 58.44982 brown     yes         Eco                1
P43  165.7316 60.83944 brown      no         Eco                1
P44  187.5520 82.27142 brown     yes    Sciences                2
P45  159.3769 57.12178 brown     yes    Sciences                4
P46  169.2394 67.80838  blue      no    Sciences                1
P47  168.8310 57.65643 brown     yes    Sciences                1
P48  162.6460 55.34621 brown      no         Eco                2
P49  169.8175 57.81468  blue      no Art/Letters                1
P50  181.3475 71.93520 brown     yes Art/Letters                0
P51  178.3130 68.81309 brown      no         Eco                1
P52  183.3218 77.83333  blue      no    Sciences                0
P53  164.6524 50.75216  blue      no Art/Letters                1
P54  178.6433 79.28756 brown      no         Eco                2
P55  169.6532 54.74572  blue      no Art/Letters                1
P56  188.5872 79.38409 brown      no    Sciences                1
P57  182.9915 72.02892 brown     yes    Sciences                2
P58  162.7387 50.35163  blue      no Art/Letters                0
P59  169.1397 62.90806 brown      no         Eco                0
P60  188.3856 71.26351 brown     yes    Sciences                1
P61  166.0699 53.79586 brown     yes    Sciences                1
P62  192.2434 83.27874 brown      no    Sciences                1
P63  168.6083 57.93809 brown     yes         Eco                1
P64  183.8382 72.78892 brown     yes    Sciences                1
P65  160.5987 52.85885 brown     yes Art/Letters                2
P66  186.9896 80.09708 brown      no         Eco                0
P67  180.1648 71.02728 green      no         Eco                0
P68  156.6753 60.67255  blue      no    Sciences                0
P69  168.2649 58.39448 green      no Art/Letters                1
P70  163.5965 59.52288 brown      no Art/Letters                0
P71  178.0747 72.46049 brown      no         Eco                0
P72  160.1538 58.92606 brown      no    Sciences                3
P73  170.4877 58.80106  blue      no Art/Letters                2
P74  189.5150 74.38827 brown      no         Eco                1
P75  161.4072 54.38962 brown      no Art/Letters                0
P76  177.7539 79.76967 brown      no         Eco                2
P77  164.4084 59.55820 brown      no    Sciences                0
P78  184.3800 74.74333 brown     yes       Other                2
P79  165.2660 54.68543 brown     yes    Sciences                2
P80  192.0525 79.73289 green     yes         Eco                1
P81  178.0660 72.39170 brown      no    Sciences                1
P82  169.3057 53.49748  blue      no Art/Letters                0
P83  188.6166 76.32341 brown     yes    Sciences                0
P84  162.7082 54.89504 brown      no         Eco                2
P85  194.0169 73.81172 brown      no    Sciences                1
P86  161.4521 59.68636 brown      no Art/Letters                1
P87  160.9725 61.27757 brown      no Art/Letters                0
P88  164.9215 56.22138  blue      no    Sciences                0
P89  175.4864 73.65827  blue     yes    Sciences                1
P90  182.8616 74.79687 brown      no    Sciences                2
P91  164.2055 59.68393 brown      no    Sciences                0
P92  178.7189 75.08759 brown      no Art/Letters                0
P93  186.2042 69.00997  blue      no    Sciences                0
P94  164.7514 58.85714 brown     yes Art/Letters                0
P95  169.7009 58.44138  blue      no    Sciences                0
P96  171.0626 63.21528 brown      no Art/Letters                2
P97  163.9292 65.67877 brown      no         Eco                0
P98  167.9866 58.93534  blue      no    Sciences                0
P99  165.9069 60.57769 brown      no    Sciences                2
P100 161.8602 59.67245 brown      no    Sciences                2
P101 166.3038 59.65035 brown     yes         Eco                3
P102 184.1869 75.93327 green      no    Sciences                0
P103 166.4414 57.34522 brown     yes Art/Letters                1
P104 173.4296 63.38222 green      no         Eco                1
P105 161.7861 64.03747 brown      no         Eco                4
P106 169.5244 55.87103 brown      no Art/Letters                1
P107 173.8025 74.93243  blue      no Art/Letters                1
P108 161.6986 57.65877 brown      no    Sciences                3
P109 189.6979 74.73235 brown     yes Art/Letters                0
P110 168.7058 58.00097  blue      no    Sciences                2
P111 170.2862 75.57279  blue      no    Sciences                2
P112 164.2079 59.43846 brown      no         Eco                1
P113 160.6499 57.33559  blue      no         Eco                4
P114 160.2727 60.80304 brown     yes         Eco                1
P115 158.8877 58.32844  blue      no    Sciences                2
P116 171.9696 61.31778 green     yes Art/Letters                4
P117 159.0964 56.62610 brown      no    Sciences                0
P118 165.2194 59.97037 brown      no Art/Letters                0
P119 167.2734 57.99677 brown      no Art/Letters                4
P120 180.5747 76.91068 brown     yes         Eco                2
P121 161.6951 61.97262 brown      no       Other                1
P122 169.1902 63.67826 brown     yes         Eco                0
```

Exercises (selection verbs)
========================================================

- Upload the dataset "data-individuals-large.txt". Be careful with the read.table() options...
- Create a new data.frame  containing only the men wearing glasses, and keeeping information only about gender, weight and glasses.
- What is the average weight of men wearing glasses?





Adding new columns or modifying columns with the dplyr verb mutate
========================================================

The same verb mutate is used for both modifying a column, or to add a new one :

- if you use it with an already existing column name, it's a modification, 
- if you use it with a new column name, it will create it. 

The verb mutate can be seen as a mapping.

Adding new columns or modifying columns with the dplyr verb mutate
========================================================

```r
# Modyfying a column (1)
# example : transforming the size from cms to meters
df <- read.table("data-individuals.txt", header=TRUE, sep="\t")
df <- df %>%
  mutate(Height = Height/100)
head(df) 
```

```
  Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters
1    Mrs 1.661982 61.63149 brown      no         Eco                4
2    Mrs 1.632252 60.15708  blue     yes         Eco                1
3    Mrs 1.681762 54.16759  blue      no    Sciences                2
4    Mrs 1.625219 57.29601 brown     yes Art/Letters                1
5     Mr 1.673222 70.50401  blue      no         Eco                1
6    Mrs 1.592243 57.80337 brown      no       Other                0
```

Adding new columns or modifying columns with the dplyr verb mutate
========================================================

```r
# Modyfying a column (2) with a conditional expression
# example : "yes"/"no" for the seeblings instead of its size
df <- df %>%
  mutate(Brothers.Sisters = ifelse(Brothers.Sisters>0, "Yes","No"))
head(df) 
```

```
  Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters
1    Mrs 1.661982 61.63149 brown      no         Eco              Yes
2    Mrs 1.632252 60.15708  blue     yes         Eco              Yes
3    Mrs 1.681762 54.16759  blue      no    Sciences              Yes
4    Mrs 1.625219 57.29601 brown     yes Art/Letters              Yes
5     Mr 1.673222 70.50401  blue      no         Eco              Yes
6    Mrs 1.592243 57.80337 brown      no       Other               No
```

Adding new columns or modifying columns with the dplyr verb mutate
========================================================

```r
# Adding a new column in the dataset
# example : adding the body mass index
df <- read.table("data-individuals.txt", header=TRUE, sep="\t")
df <- df %>%
  mutate(Height = Height/100) %>%
  mutate(bmi = Weight / Height**2)
head(df) 
```

```
  Gender   Height   Weight  Eyes Glasses       Major Brothers.Sisters      bmi
1    Mrs 1.661982 61.63149 brown      no         Eco                4 22.31260
2    Mrs 1.632252 60.15708  blue     yes         Eco                1 22.57939
3    Mrs 1.681762 54.16759  blue      no    Sciences                2 19.15183
4    Mrs 1.625219 57.29601 brown     yes Art/Letters                1 21.69205
5     Mr 1.673222 70.50401  blue      no         Eco                1 25.18295
6    Mrs 1.592243 57.80337 brown      no       Other                0 22.79999
```


Renaming a column with the dplyr verb rename
========================================================

Renaming columns is very useful (especially before or after joining tables). 

To rename a column, a simple verb rename exists (although we could have used mutate and pipe it with a select to obtain a column renaming).


Renaming a column with the dplyr verb rename
========================================================

```r
df <- read.table("data-individuals.txt", header=TRUE, sep="\t")
# Renaming a column
# example : indicating the unit for heights and weights
df <- df %>%
  mutate(Height = Height/100) %>%
  rename(height.ms = Height,
         weight.kgs = Weight) %>%
  mutate(bmi = weight.kgs / height.ms**2)
head(df) 
```

```
  Gender height.ms weight.kgs  Eyes Glasses       Major Brothers.Sisters
1    Mrs  1.661982   61.63149 brown      no         Eco                4
2    Mrs  1.632252   60.15708  blue     yes         Eco                1
3    Mrs  1.681762   54.16759  blue      no    Sciences                2
4    Mrs  1.625219   57.29601 brown     yes Art/Letters                1
5     Mr  1.673222   70.50401  blue      no         Eco                1
6    Mrs  1.592243   57.80337 brown      no       Other                0
       bmi
1 22.31260
2 22.57939
3 19.15183
4 21.69205
5 25.18295
6 22.79999
```

Sorting the dataset  with the dplyr verb arrange
========================================================


```r
# Sorting by increasing order
# example : the bmi are sorted
df <- df %>%
  arrange(bmi)
head(df)
```

```
    Gender height.ms weight.kgs  Eyes Glasses       Major Brothers.Sisters
1 NoAnswer  1.716157   49.21985 brown      no    Sciences                1
2      Mrs  1.708752   54.20974 brown      no    Sciences                4
3      Mrs  1.693057   53.49748  blue      no Art/Letters                0
4      Mrs  1.646524   50.75216  blue      no Art/Letters                1
5       Mr  1.847760   64.58050  blue     yes    Sciences                4
6      Mrs  1.627387   50.35163  blue      no Art/Letters                0
       bmi
1 16.71191
2 18.56605
3 18.66337
4 18.72054
5 18.91518
6 19.01218
```

Sorting the dataset  with the dplyr verb arrange
========================================================


```r
# Sorting by decreasing order
# example : the bmi are sorted
df <- df %>%
  arrange(desc(bmi))
head(df)
```

```
  Gender height.ms weight.kgs  Eyes Glasses       Major Brothers.Sisters
1     Mr  1.716314   84.50051  blue     yes Art/Letters                2
2     Mr  1.707774   76.67326 brown     yes    Sciences                2
3     Mr  1.730950   78.11396 brown      no    Sciences                1
4     Mr  1.702862   75.57279  blue      no    Sciences                2
5     Mr  1.733687   77.62541 brown      no         Eco                2
6     Mr  1.779655   80.28017 brown     yes Art/Letters                0
       bmi
1 28.68571
2 26.28956
3 26.07112
4 26.06191
5 25.82632
6 25.34758
```

Sorting the dataset  with the dplyr verb arrange
========================================================

```r
# Sorting by increasing order with several columns
# example : the bmi are sorted
df <- df %>%
  arrange(Major, bmi) 
head(df)
```

```
  Gender height.ms weight.kgs  Eyes Glasses       Major Brothers.Sisters
1    Mrs  1.693057   53.49748  blue      no Art/Letters                0
2    Mrs  1.646524   50.75216  blue      no Art/Letters                1
3    Mrs  1.627387   50.35163  blue      no Art/Letters                0
4    Mrs  1.696532   54.74572  blue      no Art/Letters                1
5    Mrs  1.695244   55.87103 brown      no Art/Letters                1
6    Mrs  1.725036   58.65045 brown      no Art/Letters                0
       bmi
1 18.66337
2 18.72054
3 19.01218
4 19.02068
5 19.44117
6 19.70947
```


Exercises (windowing verbs)
========================================================
- Upload the dataset "data-individuals-large.txt". Be careful with the \texttt{read.table()} options...
- Create a new data.frame  containing only the men wearing glasses, and indicating their height, weight, body mass index and another column indicating if the student should be on diet or not. The diet is required iff the bmi is under 18 (not enough) or above 25 (too much).


Summarizing values with the dplyr verb summarise
========================================================

The verb summarise (or summarize,  both spellings are OK) is used when you want an entire column to be summarised into a single value.


```r
df %>%
  summarise(
    ave.size = mean(height.ms, na.rm=TRUE),
    sd.size = sd(height.ms, na.rm=TRUE)
  )
```

```
  ave.size    sd.size
1 1.721461 0.09572842
```

Summarizing values with the dplyr verb summarise
========================================================

It is very often used with the verb group\_by that creates subgroups.

```r
df %>%
  group_by(Gender) %>%
  summarise(
    ave.size = mean(height.ms, na.rm=TRUE),
    sd.size = sd(height.ms, na.rm=TRUE)
  )
```

```
# A tibble: 3 x 3
  Gender   ave.size sd.size
  <fct>       <dbl>   <dbl>
1 Mr           1.82  0.0676
2 Mrs          1.65  0.0373
3 NoAnswer     1.73  0.0580
```



Exercises (summarising verbs)
========================================================

- Upload the dataset "data-individuals-large.txt". Be careful with the read.table() options...
- Create a new data.frame  containing, for all the persons having aswered the gender question, the median weight and the number of art students, seperately for both men and women.


Joining tables with the dplyr verb left_join
========================================================

We have two tables that were found in https://www.gapminder.org/data/: 

- the children mortality rate (death of children between 0 and 5 y.o. per 1000 born), 
- the fertility rates (children per woman), for 193 countries.

We would like to combine both...

Joining tables with the dplyr verb left_join
========================================================


```r
df_child_mort <- read.table("child_mortality.csv", header=TRUE, sep=",", 
						check.names = FALSE)
head(df_child_mort)   # ooch, this is not tidy !
```

```
              country 1800 1801 1802 1803 1804 1805 1806 1807 1808 1809 1810
1         Afghanistan  469  469  469  469  469  469  470  470  470  470  470
2             Albania  375  375  375  375  375  375  375  375  375  375  375
3             Algeria  460  460  460  460  460  460  460  460  460  460  460
4             Andorra   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA
5              Angola  486  486  486  486  486  486  486  486  486  486  486
6 Antigua and Barbuda  474  470  466  462  458  455  451  447  444  440  437
  1811 1812 1813 1814 1815 1816 1817 1818 1819 1820 1821 1822 1823 1824 1825
1  470  470  470  470  470  471  471  471  471  471  471  471  471  471  471
2  375  375  375  375  375  375  375  375  375  375  375  375  375  375  375
3  460  460  460  460  460  460  460  460  460  460  460  460  460  460  460
4   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA
5  486  486  486  486  486  486  486  486  486  486  486  486  486  486  486
6  433  430  426  423  419  416  413  409  406  403  399  396  393  390  387
  1826 1827 1828 1829 1830 1831 1832 1833 1834 1835 1836 1837 1838 1839 1840
1  473  473  473  473  473  473  473  473  473  473  474  474  474  474  474
2  375  375  375  375  375  375  375  375  375  375  375  375  375  375  375
3  460  460  460  460  460  460  460  460  460  460  460  460  460  460  460
4   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA
5  486  486  486  486  486  486  486  486  486  486  486  486  486  486  486
6  383  380  377  374  371  368  365  362  359  356  354  351  348  345  342
  1841 1842 1843 1844 1845 1846 1847 1848 1849 1850 1851 1852 1853 1854 1855
1  474  474  474  474  476  476  476  476  476  476  476  476  476  476  477
2  375  375  375  375  375  375  375  375  375  375  375  375  375  375  375
3  460  460  460  460  460  460  460  460  460  460  460  460  460  460  460
4   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA
5  486  486  486  486  486  486  486  486  486  486  486  486  486  486  486
6  340  337  334  331  329  326  323  321  318  316  313  310  308  305  303
  1856 1857 1858 1859 1860 1861 1862 1863 1864 1865 1866 1867 1868 1869 1870
1  477  477  477  477  477  477  477  477  477  479  479  479  479  479  479
2  375  375  375  375  375  375  375  375  375  375  375  375  375  375  375
3  460  460  460  460  460  460  460  460  460  460  460  460  460  460  460
4   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA
5  486  486  486  486  486  486  486  486  486  486  486  486  486  486  486
6  301  298  296  293  291  289  286  284  282  279  277  275  273  270  268
  1871 1872 1873 1874 1875 1876 1877 1878 1879 1880 1881 1882 1883 1884 1885
1  479  479  479  479  480  480  480  480  480  480  480  480  480  480  481
2  375  375  375  375  375  375  375  375  375  375  375  375  375  375  375
3  460  460  460  460  460  460  460  460  460  460  460  460  460  460  460
4   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA
5  486  486  486  486  486  486  486  486  486  486  486  486  486  486  486
6  266  264  262  260  258  256  253  251  249  247  245  243  241  239  238
  1886 1887 1888 1889 1890 1891 1892 1893 1894 1895 1896 1897 1898 1899 1900
1  481  481  481  481  481  481  481  481  483  483  483  483  482  482  482
2  375  375  375  375  375  375  375  375  375  375  375  375  375  375  374
3  460  460  460  460  460  460  460  460  460  460  460  460  460  460  459
4   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA
5  486  486  486  486  486  486  486  486  486  486  486  485  485  485  485
6  236  234  232  230  228  226  224  223  221  219  217  215  214  212  210
  1901 1902 1903 1904 1905 1906 1907 1908 1909 1910 1911 1912 1913 1914 1915
1  481  481  480  481  480  480  479  478  477  476  475  473  472  473  471
2  374  374  373  373  372  372  371  370  369  369  368  367  366  365  364
3  459  458  458  457  457  456  455  454  453  452  451  450  449  448  447
4   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA
5  484  484  483  483  482  481  480  479  478  477  476  475  474  473  471
6  209  207  205  204  202  200  199  197  195  194  192  191  189  188  186
  1916 1917 1918 1919 1920 1921 1922 1923 1924 1925 1926 1927 1928 1929 1930
1  470  469  468  467  466  465  463  462  463  461  460  459  458  457  456
2  363  362  362  361  360  359  358  357  356  355  355  354  353  352  348
3  446  445  443  442  441  440  439  438  433  429  426  421  417  412  409
4   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA
5  470  469  468  467  466  465  463  462  461  460  459  458  457  455  454
6  185  183  182  180  179  177  176  174  173  172  170  169  168  166  165
  1931 1932 1933 1934 1935 1936 1937 1938 1939 1940 1941 1942 1943 1944 1945
1  454  452  452  450  449  447  445  443  442  440  438  437  436  435  433
2  344  341  337  333  330  326  323  319  316  312  309  305  302  299  296
3  404  397  392  386  380  373  367  361  356  350  344  342  340  337  324
4   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA
5  449  444  438  433  428  423  417  412  407  402  394  387  380  371  364
6  164  163  162  161  160  159  158  157  156  155  154  153  152  151  150
  1946 1947 1948 1949 1950 1951 1952 1953 1954 1955 1956 1957 1958 1959  1960
1  431  430  428  426  425  421  414  407  400  394  387  381  375  369 364.0
2  292  289  286  283  280  269  258  248  239  229  220  211  203  195 188.0
3  311  299  287  275  264  262  258  254  250  249  248  247  246  245 245.0
4   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA   NA    NA
5  357  350  342  335  329  328  326  324  322  319  317  314  311  308 305.0
6  149  148  147  146  145  142  136  131  125  120  115  110  106  102  97.4
   1961  1962  1963  1964  1965  1966 1967  1968  1969  1970 1971  1972  1973
1 358.0 352.0 346.0 340.0 334.0 328.0  323 317.0 312.0 306.0  300 295.0 289.0
2 180.0 173.0 166.0 160.0 153.0 147.0  142 136.0 131.0 126.0  121 116.0 111.0
3 245.0 245.0 246.0 247.0 248.0 248.0  248 247.0 244.0 241.0  236 229.0 222.0
4    NA    NA    NA    NA    NA    NA   NA    NA    NA    NA   NA    NA    NA
5 302.0 299.0 295.0 291.0 287.0 284.0  280 276.0 272.0 268.0  264 260.0 257.0
6  93.5  89.7  86.1  82.6  79.3  76.1   73  70.1  67.3  64.6   62  59.5  57.2
   1974  1975  1976  1977  1978  1979  1980  1981  1982  1983  1984  1985  1986
1 283.0 277.0 271.0 264.0 258.0 252.0 245.0 238.0 232.0 225.0 218.0 211.0 204.0
2 107.0 103.0  98.7  94.8  90.8  84.1  77.8  72.0  66.8  61.9  57.4  53.4  49.8
3 213.0 204.0 194.0 184.0 173.0 161.0 148.0 133.0 117.0 101.0  86.2  73.7  64.4
4    NA    NA    NA    NA    NA    NA    NA    NA    NA    NA    NA  12.9  11.9
5 253.0 249.0 246.0 243.0 240.0 238.0 236.0 234.0 231.0 228.0 225.0 224.0 223.0
6  54.9  52.7  50.6  48.6  46.7  44.8  43.0  41.3  39.7  38.1  36.6  35.2  33.1
   1987  1988  1989  1990  1991  1992  1993  1994  1995  1996  1997  1998  1999
1 197.0 191.0 184.0 177.0 171.0 165.0 160.0 155.0 150.0 145.0 141.0 137.0 133.0
2  46.7  44.1  41.8  40.0  38.3  36.9  35.5  34.2  32.9  31.5  30.1  28.7  27.4
3  57.9  53.5  50.8  49.0  47.6  46.5  45.5  44.4  43.3  42.3  41.5  40.8  40.3
4  11.1  10.2   9.5   8.8   8.1   7.5   7.0   6.5   6.1   5.7   5.3   5.0   4.8
5 222.0 221.0 221.0 221.0 222.0 223.0 224.0 224.0 223.0 222.0 220.0 217.0 212.0
6  31.2  29.4  27.7  26.1  24.6  23.2  21.8  20.6  19.5  18.4  17.4  16.5  15.6
   2000  2001  2002  2003  2004  2005  2006  2007  2008  2009  2010  2011  2012
1 130.0 126.0 122.0 118.0 114.0 110.0 106.0 102.0  98.2  94.1  90.2  86.4  82.8
2  26.0  24.8  23.5  22.4  21.4  20.4  19.4  18.6  17.9  17.2  16.6  16.0  15.4
3  39.7  38.9  37.8  36.5  35.1  33.5  32.1  30.7  29.4  28.3  27.3  26.6  26.1
4   4.6   4.4   4.2   4.1   4.0   3.8   3.7   3.6   3.5   3.4   3.3   3.2   3.1
5 207.0 201.0 194.0 186.0 177.0 167.0 158.0 148.0 138.0 128.0 119.0 111.0 104.0
6  14.9  14.2  13.6  13.1  12.6  12.1  11.7  11.4  11.0  10.6  10.3   9.9   9.6
  2013 2014 2015 2016  2017  2018
1 79.3 76.1 73.2 70.4 68.20 65.90
2 14.9 14.4 14.0 13.5 13.30 12.90
3 25.8 25.6 25.5 25.2 23.90 23.10
4  3.0  2.9  2.8  2.7    NA    NA
5 96.8 91.2 86.5 82.5 83.10 81.60
6  9.3  9.0  8.7  8.5  8.16  7.89
```

(tidying my dataset with a pivot)
========================================================

```r
library(tidyr)
df_child_mort <- df_child_mort %>%
  pivot_longer(
    c(-"country"),
    names_to = "year", 
    values_to = "child.mort.rate")
head(df_child_mort)
```

```
# A tibble: 6 x 3
  country     year  child.mort.rate
  <fct>       <chr>           <dbl>
1 Afghanistan 1800              469
2 Afghanistan 1801              469
3 Afghanistan 1802              469
4 Afghanistan 1803              469
5 Afghanistan 1804              469
6 Afghanistan 1805              469
```

Exercise
========================================================

- Prepare a second tidy data.frame with the number of children per woman, called df_fertility.

- Then, join the 2 tables based on the country and the year with the command :


```r
#df_joined <- df_child_mort %>%
#  left_join(df_fertility, by=c("country","year")) 
#head(df_joined)
```


Note
========================================================

We have used a left join, but other joining verbs exist (right_join, inner_join, full_join).


Global Exercise
========================================================

- Upload the datasets "children per women", "child mortality" and "income" in 3 datasets.
- Create a new data.frame  joining everything.
- Add a new column "Century" that indicates the century (19,20 or 21).
- For each country, compute the average children per woman and the average income depending on the century.

