Seminar: Introduction to R for your data analysis and visualisations

- Session 1: Tuesday 28. January 2020, 9 am - 12 am
- Session 2: Thursday 30. January 2020, 2 pm - 5 pm 
- Where: Noirmoutier Room (A 030)

Have you ever felt lost for your data analysis and visualisations ? 
The following seminar/tutorial by Emmanuelle Becker is for you ! 

***
The session offers a short introduction to the use of R to manipulate and
combine data tables, and to the ggplot2 library to visualize data.

Keywords: 
- R, tidyverse libraries (tidyr, dplyr, reshape2, ggplot2),
- Classical plots (barplot, boxplot, pies, histograms, curves)
    with or without facetting.
***

Misc. notes:

- The number of participants is limited to 10 for this first session.
- Laptops aren't mandatory: the workstations of the Noirmoutier room will 
  be used for the hands-on exercices.
- For PhD students, this seminar can be validated as doctoral courses

